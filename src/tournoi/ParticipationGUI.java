package tournoi;

import javax.swing.*;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.*;

class ParticipationGUI {
	private JButton btn_ok = new JButton("OK");
	private JComboBox cbox_student = new JComboBox();
	private JComboBox cbox_tournoi = new JComboBox();
	private JLabel lbl_student = new JLabel("Etudiant");
	private JLabel lbl_tournoi = new JLabel("Tournoi");
	private ArrayList<String> arr_student = new ArrayList<String>();
	private ArrayList<String> arr_tournoi = new ArrayList<String>();
	
	public ParticipationGUI() {
		Jdbc jdbc_conn = new Jdbc(); // connecting to DB		
		// getting data
		try {
			arr_student = jdbc_conn.getAllStudents();
			arr_tournoi = jdbc_conn.getAllTournois();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		if(arr_student != null && arr_tournoi != null) {

			JFrame frame = new JFrame("Ajouter une Participation");
			JPanel pane = new JPanel(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();

			c.weightx = 0.8;
			c.weighty = 0.25;
		    c.gridy = 1;
		    c.gridheight = 1;
		    c.gridx = 1;
		    c.ipadx = 1;
		    c.ipady = 1;
		    
			pane.add(lbl_student, c);
			c.gridx = 2;
			pane.add(lbl_tournoi, c);
			
			for(String s : arr_student) 
				cbox_student.addItem(s);
			
			c.gridy = 2;
		    c.gridheight = 1;
		    c.gridx = 1;
		    c.ipadx = 1;
		    c.ipady = 1;
//			c.fill = GridBagConstraints.HORIZONTAL;
			pane.add(cbox_student, c);
			
			for(String s : arr_tournoi) 
				cbox_tournoi.addItem(s);
			
			c.gridy = 2;
		    c.gridheight = 1;
		    c.gridx = 2;
		    c.ipadx = 1;
		    c.ipady = 1;
			pane.add(cbox_tournoi, c);
			
			c.gridy = 3;
		    c.gridheight = 1;
		    c.gridx = 1;
		    c.ipadx = 1;
		    c.ipady = 1;
		
		    btn_ok.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					try {
						String cne = jdbc_conn.getCNEof(cbox_student.getSelectedItem().toString());
						int numero = jdbc_conn.getNumberof(cbox_tournoi.getSelectedItem().toString());
						jdbc_conn.insertNewParticipation(cne, numero);
						JOptionPane.showMessageDialog(null, "Nouveau Participation a ete ajoute avec Succes", "Succes", JOptionPane.INFORMATION_MESSAGE);
						MainGUI.refresh();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
		    
			pane.add(btn_ok, c);
			frame.getContentPane().add(pane, BorderLayout.CENTER);
			
			frame.setBounds(700, 400, 300, 150);
			frame.setVisible(true);
			
		}
		else {
			JOptionPane.showMessageDialog(null, "Base de donnees est vide !", "Erreur", JOptionPane.ERROR_MESSAGE);
		}
				
	}
}
