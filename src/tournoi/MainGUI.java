package tournoi;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import java.awt.BorderLayout;
import java.awt.event.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

class MainGUI {
	private static JTable table;
	private static Jdbc jdbc_conn;
	private JButton btn_newStudent, btn_newTournoi, btn_newParticipation;
	
	public MainGUI() {
		jdbc_conn = new Jdbc(); // connecting to DB
		
		JFrame frame = new JFrame("Les Participations");
		table = jdbc_conn.getTableData(); // getting a JTable that is populated with data from DB
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		
		btn_newStudent = new JButton("Ajout Etudiant");
		btn_newTournoi = new JButton("Ajout Tournoi");
		btn_newParticipation = new JButton("Ajout Participation");
		
		MyActionListener action = new MyActionListener();
		
		btn_newStudent.addActionListener(action); btn_newTournoi.addActionListener(action); btn_newParticipation.addActionListener(action);
		
//		table.setSize(10, 10);
		JScrollPane tableSP = new JScrollPane(table);
		
		JPanel p_table = new JPanel();
		JPanel p_buttons = new JPanel();
		JPanel p_main = new JPanel(new BorderLayout());
		p_table.add(tableSP);
		p_buttons.add(btn_newStudent); p_buttons.add(btn_newTournoi); p_buttons.add(btn_newParticipation);
		p_main.add(p_table, BorderLayout.NORTH);
		p_main.add(p_buttons, BorderLayout.SOUTH);
		frame.getContentPane().add(p_main, BorderLayout.CENTER);
					
		frame.setBounds(500, 200, 800, 600);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {
		MainGUI m = new MainGUI();
	}

	private class MyActionListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			if (event.getSource() == btn_newStudent) {
				new StudentGUI();
			}
			else if (event.getSource() == btn_newTournoi) {
				new TournoiGUI();
			}
			else if (event.getSource() == btn_newParticipation) {
				new ParticipationGUI();
			}
		}
	}
	
	public static void refresh() {
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		model.setRowCount(0);
		Statement statement;
		ResultSet rs;
		try {
			statement = jdbc_conn.getConn().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = statement.executeQuery("SELECT E.nom, prenom, age, T.nom, type FROM Etudiant E, Tournoi T, Participe P WHERE " + 
					  "P.cne = E.cne AND P.numero = T.numero UNION SELECT distinct(E.nom), E.prenom, E.age, ' ' " + 
					  "AS member_role, ' ' AS member_type FROM Etudiant E, Participe P WHERE E.cne NOT in (select cne " +
					  "from Participe);");
			//rs = statement.executeQuery("SELECT E.nom, prenom, age, T.nom, type FROM Etudiant E, Tournoi T, Participe P WHERE P.cne = E.cne AND P.numero = T.numero UNION SELECT distinct(E.nom), E.prenom, E.age, ' ' AS member_role, ' ' AS member_type FROM Etudiant E, Participe P WHERE E.cne NOT in (select cne from Participe) UNION SELECT distinct(E.nom), E.prenom, E.age, ' ' AS member_role, ' ' AS member_type FROM Etudiant E");
			table.setModel(Jdbc.buildTableModel(rs));

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
