package tournoi;

import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.*;
import java.sql.SQLException;

class StudentGUI {
	
	private JButton btn_ok = new JButton("OK");
	private JLabel[] labels = new JLabel[] {new JLabel("CNE: "), new JLabel("Nom: "), new JLabel("Prenom: "), new JLabel("Age: ")};
	private JTextField[] textfields = new JTextField[4];
	
	public StudentGUI() {
		Jdbc jdbc_conn = new Jdbc(); // connecting to DB		
		JFrame frame = new JFrame("Ajouter un etudiant");
		JPanel pane = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		for (int i = 0; i < 4; i++) textfields[i] = new JTextField(50); 
		
		btn_ok.addActionListener(new ActionListener() { 
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				boolean good_to_go = true;
				boolean isInt = false;
				
				try {
					Integer.parseInt(textfields[3].getText());
					isInt = true;
				} catch (NumberFormatException ex) {
					isInt = false;
				}
				
				if( isInt ) {
					for(int i = 0; i < 3; i++) {
						int len = textfields[i].getText().trim().length();
						
						if(len > 50 || len <= 0) {
							String msg;
							
							if(len > 50)
								msg = "Le texte entre ne peut pas depasser 50 caracteres !";
							else 
								msg = "Le texte entre ne peut pas etre vide !";
							
							switch(i) {
								case 0:
									msg = "CNE: " + msg;
									break;
								case 1:
									msg = "Nom: " + msg;
									break;
								case 2:
									msg = "Prenom: " + msg;
									break;
							}
							JOptionPane.showMessageDialog(null, msg, "Erreur", JOptionPane.ERROR_MESSAGE);
							good_to_go = false;
							break;
						}
					}
				}
				else {
					good_to_go = false;
					JOptionPane.showMessageDialog(null, "L'age doit etre un nombre", "Erreur", JOptionPane.ERROR_MESSAGE);
				}
				
				
				if(good_to_go) { // need to check for duplicate key
					
					try {
						jdbc_conn.insertNewStudent(textfields[0].getText(), textfields[1].getText(), textfields[2].getText(), Integer.parseInt(textfields[3].getText()));
						JOptionPane.showMessageDialog(null, "Nouveau Etudiant a ete ajoute avec Succes", "Succes", JOptionPane.INFORMATION_MESSAGE);
						reset();
						MainGUI.refresh();
					} catch (SQLException ex) {
						if(ex.getMessage().startsWith("Duplicate"))
							JOptionPane.showMessageDialog(null, "Le CNE deja existe. Veuillez Entrer un nouveau CNE", "Erreur", JOptionPane.ERROR_MESSAGE);
						else
							JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});

		c.weightx = 0.2;
		c.weighty = 0.25;
		for (int row = 1; row <= 5; row++) {
		    c.gridy = row;
		    c.gridheight = 1;
		    
		    if(row < 5)
		    	for (int column = 1; column <= 2; column++) {
			        c.gridx = column;
			        c.ipadx = 1;
			        c.ipady = 1;
			        
			        if (column == 1) {
			        	c.fill = GridBagConstraints.CENTER;
			        	pane.add(labels[row-1], c);
			        }
			        else {
			        	c.fill = GridBagConstraints.HORIZONTAL;
			        	pane.add(textfields[row-1], c);
			        }
			        	
			    }
		    
		    else {
		    	c.gridx = 1;
		    	c.fill = GridBagConstraints.CENTER;
		    	pane.add(btn_ok, c);
		    }
		}
		
		frame.getContentPane().add(pane, BorderLayout.CENTER);
		
		frame.setBounds(500, 200, 400, 300);
		frame.setVisible(true);
	}
	
	private void reset() {
		for (int i = 0; i < 4; i++)
			textfields[i].setText("");
	}

}
