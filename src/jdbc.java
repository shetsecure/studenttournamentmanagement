

import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Vector;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

class Jdbc {
	private Connection conn;
	private Scanner s = new Scanner(System.in);
	private String user = "shetsecure", password = "123456", db = "tournoi";
	private Statement statement;
	private boolean connected = false;
	
	public Jdbc() {
		if(connected == false) {
			checkJdbcDriver();
			//setCredentials();
			connectDB();
			connected = true;
		}
		
//		if(connected)
//			System.out.println("mzian");
	}
	
	private void connectDB() {
		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + db, user, password);
		} catch (SQLException e) {
			displaySQLError(e);
			System.exit(2);
		}
	}
	
	private void displaySQLError(SQLException e) {
		System.out.println("SQLException: " + e.getMessage());
		System.out.println("SQLState: " + e.getSQLState());
		System.out.println("Error Code: " + e.getErrorCode());
	}
	
	private void checkJdbcDriver() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
		} catch(Exception e) {
			System.out.println("Cannot load mysql Driver.");
			System.out.println(e.getMessage());
			System.exit(1);
		}
	}
	
	public void setCredentials() {
		System.out.println("Enter database name: ");
		db = s.nextLine();
		System.out.println("Enter username: ");
		user = s.nextLine();
		System.out.println("Enter password: ");
		password = s.nextLine();
	}
	
	public void insertNewTournoi(long n, String nom, String type) throws SQLException {
		statement = conn.createStatement();
		statement.executeUpdate( "INSERT INTO Tournoi Values(" + n + ",' " + nom + "'," + "'" + type + "')" );
	}
	
	public void insertNewStudent(String cne, String nom, String prenom, int age) throws SQLException {
		statement = conn.createStatement();
		statement.executeUpdate( "INSERT INTO Etudiant Values(" + "'" + cne + "'," + "'" + nom + "',"  + "'" + prenom + "'," + age + ")");
	}
	
	public int insertNewParticipation(String cne, int numero) throws SQLException { // no need to check, will use combobox
		statement = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		ResultSet rs = statement.executeQuery("SELECT * FROM Participe WHERE cne = '" + cne + "' AND numero = " + numero);
		int size = 0; rs.last(); size = rs.getRow(); // checking if it's already exists.
		
		if(size > 0) // existe
			return size;
		
		else 	
			statement.executeUpdate( "INSERT INTO Participe(cne, numero) Values('" + cne + "', " + numero + ")");
		
		return size;
	}
	
	public static DefaultTableModel buildTableModel(ResultSet rs) throws SQLException {

	    ResultSetMetaData metaData = rs.getMetaData();

	    // getting names of the columns selected
	    Vector<String> columnNames = new Vector<String>();
	    int len = metaData.getColumnCount();
	    for (int i = 1; i <= len; i++) 
	        columnNames.add(metaData.getColumnName(i));
	    

	    // data of the table
	    Vector<Vector<Object>> data = new Vector<Vector<Object>>();
	    while (rs.next()) {
	        Vector<Object> vector = new Vector<Object>();
	        
	        for (int i = 1; i <= len; i++) 
	            vector.add(rs.getObject(i));
	        
	        data.add(vector);
	    }

	    return new DefaultTableModel(data, columnNames);

	}
	
	public JTable getTableData() {
		try {
			statement = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			/*ResultSet rs = statement.executeQuery("SELECT E.nom, prenom, age, T.nom, type FROM Etudiant E, Tournoi T, Participe P WHERE "
												+ "P.cne = E.cne AND P.numero = T.numero");*/
			
			ResultSet rs = statement.executeQuery("SELECT E.nom, prenom, age, T.nom, type FROM Etudiant E, Tournoi T, Participe P WHERE " + 
												  "P.cne = E.cne AND P.numero = T.numero UNION SELECT distinct(E.nom), E.prenom, E.age, ' ' " + 
												  "AS member_role, ' ' AS member_type FROM Etudiant E, Participe P WHERE E.cne NOT in (select cne " +
												  "from Participe);");
			
			//ResultSet rs = statement.executeQuery("SELECT E.nom, prenom, age, T.nom, type FROM Etudiant E, Tournoi T, Participe P WHERE P.cne = E.cne AND P.numero = T.numero UNION SELECT distinct(E.nom), E.prenom, E.age, ' ' AS member_role, ' ' AS member_type FROM Etudiant E, Participe P WHERE E.cne NOT in (select cne from Participe) UNION SELECT distinct(E.nom), E.prenom, E.age, ' ' AS member_role, ' ' AS member_type FROM Etudiant E");
			
			JTable table = new JTable(buildTableModel(rs));
			
			return table;
		} catch (SQLException e) {
			displaySQLError(e);
			return null;
		}
	}
	
	public ArrayList<String> getAllStudents() throws SQLException {
		ArrayList<String> students = new ArrayList<String>();
		
		statement = conn.createStatement();
		ResultSet rs = statement.executeQuery("SELECT * FROM Etudiant");
		
		if(rs.next()) {
			rs.beforeFirst();
			while(rs.next())
				students.add(rs.getString(2));
			return students;
		}
		
		return null;
	}
	
	public ArrayList<String> getAllTournois() throws SQLException {
		ArrayList<String> tournois = new ArrayList<String>();
		
		statement = conn.createStatement();
		ResultSet rs = statement.executeQuery("SELECT * FROM Tournoi");
		
		if(rs.next()) {
			rs.beforeFirst();
			while(rs.next())
				tournois.add(rs.getString(2));
			return tournois;
		}
		
		return null;
	}
	
	public String getCNEof(String name) throws SQLException {
		
		statement = conn.createStatement();
		ResultSet rs = statement.executeQuery("SELECT * FROM Etudiant WHERE nom = '" + name + "' LIMIT 1");
		
		if(rs.next()) {
			rs.beforeFirst();
			while(rs.next())
				return rs.getString(1);
		}
		
		return null;
	}

	public int getNumberof(String name) throws SQLException {
		
		statement = conn.createStatement();
		ResultSet rs = statement.executeQuery("SELECT * FROM Tournoi WHERE nom = '" + name + "'");
		
		if(rs.next()) {
			rs.beforeFirst();
			
			while(rs.next())
				return  Integer.parseInt((rs.getString(1)));
		}
		
		return -1;
	}

	public Connection getConn() {
		return conn;
	}
	
	@Override
	public void finalize() {
	    try {
	        s.close();
	        statement.close();
	        conn.close();
	    } catch (Exception e) {
	        System.out.println(e.getMessage());
	    }
	}
}
