-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 03, 2019 at 01:18 AM
-- Server version: 10.1.35-MariaDB-1
-- PHP Version: 7.2.9-1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tournoi`
--

-- --------------------------------------------------------

--
-- Table structure for table `Etudiant`
--

CREATE TABLE `Etudiant` (
  `cne` varchar(50) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `age` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Etudiant`
--

INSERT INTO `Etudiant` (`cne`, `nom`, `prenom`, `age`) VALUES
('2019', '2019', '2019', 20),
('986876y', 'iuyiuy', 'iuy', 748),
('dsiauy', 'fdshfdsiuh', 'dfsuihfiduh', 456),
('N1234556', 'amine', 'marzouki', 26),
('N12423', 'no', 'no', 29);

-- --------------------------------------------------------

--
-- Table structure for table `Participe`
--

CREATE TABLE `Participe` (
  `id` int(10) UNSIGNED NOT NULL,
  `cne` varchar(50) NOT NULL,
  `numero` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Participe`
--

INSERT INTO `Participe` (`id`, `cne`, `numero`) VALUES
(13, 'N1234556', 1),
(14, 'dsiauy', 1),
(15, '986876y', 1),
(16, '2019', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Tournoi`
--

CREATE TABLE `Tournoi` (
  `numero` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Tournoi`
--

INSERT INTO `Tournoi` (`numero`, `nom`, `type`) VALUES
(1, ' Foot', 'universite'),
(89, ' daskj', 'dajsh'),
(452, ' aaaaaaaaaa', 'aaaaaaaa');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Etudiant`
--
ALTER TABLE `Etudiant`
  ADD PRIMARY KEY (`cne`);

--
-- Indexes for table `Participe`
--
ALTER TABLE `Participe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cne_participe` (`cne`),
  ADD KEY `numero_participe` (`numero`);

--
-- Indexes for table `Tournoi`
--
ALTER TABLE `Tournoi`
  ADD PRIMARY KEY (`numero`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Participe`
--
ALTER TABLE `Participe`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Participe`
--
ALTER TABLE `Participe`
  ADD CONSTRAINT `cne_participe` FOREIGN KEY (`cne`) REFERENCES `Etudiant` (`cne`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `numero_participe` FOREIGN KEY (`numero`) REFERENCES `Tournoi` (`numero`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
