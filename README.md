# StudentTournamentManagement

A small java GUI project for managing students that want to participate in a tournament.

<h1> ScreenShots </h1>

<h2> Class Diagram </h2>
<img src="screens/diagram.png" />

<h2> Demo </h2>
<ol>
    <li><img src="screens/1.png" /></li>
    <li><img src="screens/2.png" /></li>
    <li><img src="screens/3.png" /></li>
    <li><img src="screens/4.png" /></li>
    <li><img src="screens/5.png" /></li>
</ol>